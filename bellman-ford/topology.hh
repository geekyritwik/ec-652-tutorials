#include <stdlib.h>

void readTopology(const char* name,int *** topology,int * V, int *E){
  FILE * file = fopen(name,"r");
  if(!file) return;
  fscanf(file,"%d %d\n",V,E);
  *topology = new int *[*V];
  for(int i=0;i<*V;i++) (*topology)[i] = new int[*V];
  for(int i=0;i<*V;i++){
    for(int j=0;j<*V;j++){
      fscanf(file,"%d ",&(*topology)[i][j]);
    }
  }
  fclose(file);
}
