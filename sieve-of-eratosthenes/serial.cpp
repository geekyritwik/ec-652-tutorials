#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <omp.h>
#include <string.h>

int main(int argc,char ** argv){
  if(argc<2){
    printf("Use: %s %s\n",argv[0],"upperbound");
    exit(1);
  }

  double t1,t2;
  int limit = atoi(argv[1]);
  int sqrtN = sqrt(limit);

  bool * array = (bool *)malloc(limit * sizeof(bool));
  memset(array,1,sizeof(bool) * limit);

  t1 = omp_get_wtime();
  int i,j;
  for(i=2;i<=sqrtN;i++){
    if(array[i] == 1){
      int sq = i * i;
      for(j = sq;j<limit;j += i){
        array[j] = 0;
      }
    }
  }

  int sol =0;
  for(i=2;i<limit;i++){
    if(array[i]==1){
      sol++;
    }
  }

  t2 = omp_get_wtime();

  double diff = t2-t1;
  printf("No of primes in range (2,%d): %d\n",limit,sol);
  printf("Time %.16g\n",diff );

  free(array);

  return 0;
}
