#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <string.h>
#include <time.h>
 
int main(int argc ,char ** argv)
{
  if(argc <3){
    printf("Usage %s %s %s\n",argv[0],"parallel (y or n)","array_size");
  }
  int *array, *partial, *temp;
  int no_of_threads;
  int divs;
  int n;
  int i, tid, last;
  char p;
  p = argv[1][0];
  printf("%c\n",p);

  n = atoi(argv[2]);
  
  if(!(array = (int *) malloc (sizeof (int) * n))) return -1;

  for(i = 0; i < n; i++)array[i] = i+1;
     clock_t    start;
     start = clock();
     double t1,t2;
  

  t1 = omp_get_wtime();
#pragma omp parallel default(none) private(i, tid, last) shared(array, partial, temp, no_of_threads, divs, n,p)
  {
#pragma omp single
    {
      
      if(p=='y')
      no_of_threads = omp_get_num_threads();
      else no_of_threads=1;

      if(!(partial = (int *) malloc (sizeof (int) * no_of_threads))) exit(-1);
      if(!(temp = (int *) malloc (sizeof (int) * no_of_threads))) exit(-1);
      divs = n / no_of_threads + 1;
    }
    tid = omp_get_thread_num();
    for(i = divs * tid + 1; i < divs * tid + divs && i < n; i++)
      array[i] += array[i - 1];
    partial[tid] = array[i - 1];
#pragma omp barrier
    for(i = 1; i < no_of_threads; i <<= 1) {
      if(tid >= i)
        temp[tid] = partial[tid] + partial[tid - i];
#pragma omp barrier
#pragma omp single
      memcpy(partial + 1, temp + 1, sizeof(int) * (no_of_threads - 1));
    }
    for(i = divs * tid; i < (last = divs * tid + divs < n ? divs * tid + divs : n); i++)
      array[i] += partial[tid] - array[last - 1];
  }
    t2 = omp_get_wtime();
double t = (clock() - start) / (double) (CLOCKS_PER_SEC / 1000)/no_of_threads;
//double t = t2 -t1;

printf( "Time: %.16g ms \n" , t);
  for(i = 0; i < n; i++)
    printf("%d ", array[i]);
  printf("\n");
  return 0;
}
