#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include <unistd.h>

#define null NULL

using namespace std;

struct Node{
  int arrayNode;
  int val;
  int next;
  char next_type;
};

struct TreeNode{
  TreeNode * parent;
  TreeNode * left;
  TreeNode * right;
  int depth;
};

struct ArrayNode{
  Node A;
  Node B;
  Node C;
};

void printInorderDepths(ArrayNode * l,int index,int n);

void traverse(ArrayNode * l,int n);

int leftChild(int i);

int rightChild(int i);

int parent(int i);

bool isLeft(int i);

void inorder(TreeNode* node);

double run_parallel(int n);

double run_serial(int n);

double list_ranking(ArrayNode * list,int n);

void recurr(TreeNode * root,int depth);

void cleanTree(TreeNode ** root);

void leftCreate(TreeNode ** root);

void rightCreate(TreeNode ** root);

void buildTree(TreeNode** root,int h);

int main(int argc,char ** argv){

  if(argc <3){
    printf("Usage: %s -<p|s> depth\n",argv[0]);
    exit(0);
  }
  char choice;
  bool parallel = false;
  int n = atoi(argv[2]);
  sscanf(argv[1],"-%c",&choice);
  if(choice == 'p') parallel = true;
  else if(choice != 's'){
    printf("Usage: %s -<p|s> depth\n",argv[0]);
    exit(0);
  }

  if(parallel){
    int k = (int)pow(2,n+1);
    printf("\nParallel Execution Time : %.20g\n",run_parallel(k-1));
  }else{
    printf("Serial Execution Time : %.20g\n",run_serial(n));
  }



  return 0;
}

double run_parallel(int n){
  double exTime = 0.0;
  ArrayNode * list = new ArrayNode[n];

  omp_set_num_threads(n);
  #pragma omp parallel shared(n)
  {
    // Assuming the number of threads have been created.
    int i = omp_get_thread_num();
    list[i].A.arrayNode =i;
    list[i].B.arrayNode =i;
    list[i].C.arrayNode =i;
    //For Processor A
    list[i].A.val = -1;
    if(leftChild(i)<n){
      list[i].A.next = leftChild(i);
      list[i].A.next_type = 'A';
    }else{
      list[i].A.next = i;
      list[i].A.next_type = 'B';
    }


    //For Processor B
    list[i].B.val = 0;
    if(rightChild(i)<n){
      list[i].B.next = rightChild(i);
      list[i].B.next_type = 'A';
    }else{
      list[i].B.next = i;
      list[i].B.next_type = 'C';
    }

    //For Processor C
    list[i].C.val = 1;
    if(i == 0){
      list[i].C.next = i;
      //TODO
      list[i].C.next_type = '-';
    }else if(isLeft(i)){
      list[i].C.next = parent(i);
      list[i].C.next_type = 'B';
    }else{
      list[i].C.next = parent(i);
      list[i].C.next_type = 'C';
    }
  }

  exTime = list_ranking(list,n);

  printInorderDepths(list,0,n);
  delete [] list;

  return exTime;
}

double run_serial(int n){
  TreeNode * root = new TreeNode();
  root->parent = null;
  buildTree(&root,n);

  double t1,t2;
  t1 = omp_get_wtime();
  recurr(root,0);


  inorder(root);
  printf("\n");

  cleanTree(&root);

  t2 = omp_get_wtime();


  return t2-t1;
}


void leftCreate(TreeNode **root){
  TreeNode * node = new TreeNode();
  (*root)->left = node;
  node->parent = *root;
  node->left = null;
  node->right = null;

}

void rightCreate(TreeNode **root){
  TreeNode * node = new TreeNode();
  (*root)->right = node;
  node->parent = *root;
  node->left = null;
  node->right = null;

}


void recurr(TreeNode * root,int depth){
  if(root == null) return;
  root->depth = depth;
  recurr(root->left,depth+1);
  recurr(root->right,depth+1);
}

void inorder(TreeNode * node){
  if(node == null) return;
  inorder(node->left);
  printf("%d ",node->depth);
  usleep(1000);
  inorder(node->right);
}

void cleanTree(TreeNode ** root){
  if(*root == null)return;
  cleanTree(&(*root)->left);
  cleanTree(&(*root)->right);
  (*root)->parent = null;
  (*root)->left = null;
  (*root)->right = null;
  delete *root;
}

void buildTree(TreeNode ** root, int h){
  if(h == 0 || *root== null)return;
  leftCreate(root);
  rightCreate(root);
  buildTree(&(*root)->left,h-1);
  buildTree(&(*root)->right,h-1);
}

int leftChild(int i){
  return 2*i+1;
}

int rightChild(int i){
  return 2*i+2;
}

int parent(int i){
  return (i-1)/2;
}

bool isLeft(int i){
  int p = parent(i);
  if(i == leftChild(p)) return true;
  else return false;
}

void traverse(ArrayNode* list,int n){
  Node temp;

  for(int i=0;i<n;i++){
    printf("%d %d %c\n",list[i].A.val,list[i].A.next,list[i].A.next_type);
    printf("%d %d %c\n",list[i].B.val,list[i].B.next,list[i].B.next_type);
    printf("%d %d %c\n",list[i].C.val,list[i].C.next,list[i].C.next_type);
    printf("*\n");
  }

  printf("****************\n");
}

// Here n is no of nodes
double list_ranking(ArrayNode * list ,int n){
  double t1,t2;

  t1 = omp_get_wtime();


  omp_set_num_threads(3*n);

  int threads =0;
  int depth = (int) ceil(log2(3*n));

  for(int j =0;j<depth;j++){
    #pragma omp parallel shared (n,j,threads,list)
    {
      #pragma omp single
      {
        if(j == 0){
          threads = omp_get_num_threads();
          printf("Threads :%d\n",threads);
        }
      }

      int i = omp_get_thread_num();

      int pos,next,next_type,type,dex;

      dex = i/3;
      type = i %3;

      switch(type){
        case 0:
        switch(list[dex].A.next_type){
          case 'A':
            pos = list[dex].A.val + list[list[dex].A.next].A.val;
            next = list[list[dex].A.next].A.next;
            next_type = list[list[dex].A.next].A.next_type;
            break;
          case 'B':
          pos = list[dex].A.val + list[list[dex].A.next].B.val;
          next = list[list[dex].A.next].B.next;
          next_type = list[list[dex].A.next].B.next_type;
          break;
          case 'C':
          pos = list[dex].A.val + list[list[dex].A.next].C.val;
          next = list[list[dex].A.next].C.next;
          next_type = list[list[dex].A.next].C.next_type;
          break;
          case '-':
          pos = list[dex].A.val;
          next = list[dex].A.next;
          next_type = list[dex].A.next_type;
          break;
        }
        break;
        case 1:
        switch(list[dex].B.next_type){
          case 'A':
            pos = list[dex].B.val + list[list[dex].B.next].A.val;
            next = list[list[dex].B.next].A.next;
            next_type = list[list[dex].B.next].A.next_type;
            break;
          case 'B':
          pos = list[dex].B.val + list[list[dex].B.next].B.val;
          next = list[list[dex].B.next].B.next;
          next_type = list[list[dex].B.next].B.next_type;
          break;
          case 'C':
          pos = list[dex].B.val + list[list[dex].B.next].C.val;
          next = list[list[dex].B.next].C.next;
          next_type = list[list[dex].B.next].C.next_type;
          break;
          case '-':
          pos = list[dex].B.val;
          next = list[dex].B.next;
          next_type = list[dex].B.next_type;
          break;
        }
        break;
        case 2:
        switch(list[dex].C.next_type){
          case 'A':
            pos = list[dex].C.val + list[list[dex].C.next].A.val;
            next = list[list[dex].C.next].A.next;
            next_type = list[list[dex].C.next].A.next_type;
            break;
          case 'B':
          pos = list[dex].C.val + list[list[dex].C.next].B.val;
          next = list[list[dex].C.next].B.next;
          next_type = list[list[dex].C.next].B.next_type;
          break;
          case 'C':
          pos = list[dex].C.val + list[list[dex].C.next].C.val;
          next = list[list[dex].C.next].C.next;
          next_type = list[list[dex].C.next].C.next_type;
          break;
          case '-':
          pos = list[dex].C.val;
          next = list[dex].C.next;
          next_type = list[dex].C.next_type;
          break;
        }
        break;
      }

      #pragma omp barrier
      {
        switch(type){
          case 0:
          list[dex].A.val = pos;
          list[dex].A.next = next;
          list[dex].A.next_type = next_type;
          break;
          case 1:
          list[dex].B.val = pos;
          list[dex].B.next = next;
          list[dex].B.next_type = next_type;
          break;
          case 2:
          list[dex].C.val = pos;
          list[dex].C.next = next;
          list[dex].C.next_type = next_type;
          break;
        }
      }


    }

  }

  t2 = omp_get_wtime();


  return t2 -t1;

}

void printInorderDepths(ArrayNode* list,int index,int n){
  if(index>=n) return;
  printInorderDepths(list,leftChild(index),n);
  printf("%d ",list[index].A.val);
  printInorderDepths(list,rightChild(index),n);
}
