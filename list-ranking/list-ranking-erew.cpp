#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
using namespace std;

struct Node{
  int val;
  int next;
};


double run_parallel(int n);

double run_serial(int n);

int main(int argc,char ** argv){

  if(argc <3){
    printf("Usage: %s -<p|s> number\n",argv[0]);
    exit(0);
  }
  char choice;
  bool parallel = false;
  int n = atoi(argv[2]);
  sscanf(argv[1],"-%c",&choice);
  if(choice == 'p') parallel = true;
  else if(choice != 's'){
    printf("Usage: %s -<p|s> number\n",argv[0]);
    exit(0);
  }

  if(parallel){
    printf("Parallel Execution Time : %.20g\n",run_parallel(n));
  }else{
    printf("Serial Execution Time : %.20g\n",run_serial(n));
  }
  return 0;
}

double run_parallel(int n){

    double t1,t2;

    Node * list = new Node[n];
    Node * temp = new Node[n-1];

    t1 = omp_get_wtime();

    #pragma omp parallel for schedule(dynamic)
    for(int i=0;i<n;i++){
      if(i == n-1){
        list[i].val =0;
        list[i].next = i;
      }else{
        list[i].val =1;
        list[i].next = i+1;
      }
    }

    #pragma omp parallel for schedule(dynamic)
    for(int i=1;i<n;i++){
       temp[i-1].val = list[i].val;
       temp[i-1].next = list[i].next;
    }

    omp_set_num_threads(n);

    int threads =0;
    int depth = (int) ceil(log2(n));

    for(int j =0;j<depth;j++){
      #pragma omp parallel shared (j,threads,n)
      {
        #pragma omp single
        {
          if(j == 0){
            threads = omp_get_num_threads();
            printf("Threads :%d\n",threads);
          }
        }

        int i = omp_get_thread_num();

        int pos,next;
        if(i == n-1){
          pos = list[i].val;
          next = i;
        }else if(list[i].next == n-1){
          pos = list[i].val;
          next = list[i].next;
        }
        else{
          pos = list[i].val + temp[i].val;
          next = temp[i].next;

        }

        #pragma omp barrier
        {
          list[i].val  = pos;
          list[i].next = next;
          int p = (int)pow(2,j+1);
          if(i-p>=0){
            temp[i-p].val = pos;
            temp[i-p].next = next;
          }
        }
      }
    }


    t2 = omp_get_wtime();

    for(int i =0;i<n;i++){
      printf("List[%d,%d] ",list[i].val,list[i].next);
    }
    printf("\n");

    delete [] list;

    return t2 -t1;

}

double run_serial(int n){

  double t1,t2;
  Node * list = new Node[n];

  t1 = omp_get_wtime();

  for(int i=0;i<n;i++){
    if(i == n-1){
      list[i].val = 0;
      list[i].next = i;
    }else{
      list[i].val =1;
      list[i].next = i+1;
    }
  }

  for(int i = n-1;i>0;i--){
    list[i-1].val = list[i].val +1;
    list[i-1].next = list[i].next;
  }

  t2 = omp_get_wtime();

  for(int i =0;i<n;i++){
    printf("List[%d,%d] ",list[i].val,list[i].next);
  }
  printf("\n");

  delete [] list;

  return t2-t1;
}
