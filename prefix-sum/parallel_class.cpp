#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include <sys/time.h>

float timedifference_msec(struct timeval t0, struct timeval t1)
{
  return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}

int main(int argc ,char ** argv){

  //int array[] ={1,2,3,4,5,6,7,8,9,10};
  int len = atoi(argv[1]);
  int * array = (int *) malloc(len * sizeof(int));
  for(int i=0;i<len;i++){
    array[i] = i+1;
  }
  int d = ceil(log2(len));
  double t1,t2;
  
  int no_of_threads;

  t1 = omp_get_wtime();
  omp_set_num_threads(len-1);


  for(int i=0;i<d;i++){
    #pragma omp parallel
  {
    #pragma omp single
  {
    if(i==0){
      no_of_threads = omp_get_num_threads();
    }
  }
      int tid = omp_get_thread_num()+1;

      int v1 =0,v2=0;
      int p = (int)pow(2,i);

      if(tid - p >=0){
        v1 = array[tid];
        v2 = array[tid -p];
      }


      #pragma omp barrier
      {
        if(tid-p>=0){
          array[tid] = v1+v2;
          }

      }

    }

  }

  t2 = omp_get_wtime();

  double diff = t2 -t1;

  for(int j=0;j<len;j++){
    printf("%d\n",array[j]);
  }

  printf("Time : %.16g\n",diff);

  delete [] array;
  return 0;
}
