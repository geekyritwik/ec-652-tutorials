#include <omp.h>

double matrix_multiplication_serial(int** matrix1,int** matrix2,const int n1,const int m1,const int n2,const int m2,int *** out){
  double t1,t2;
  if(m1 != n2){
    return -1;
  }
  *out = new int * [n1];
  for(int i=0;i<n1;i++) (*out)[i] = new int[m2];

  t1 = omp_get_wtime();

  for(int i=0;i<n1;i++){
    for(int j=0;j<m2;j++){
      int v =0;
      for(int k=0;k<m1;k++){
        v += matrix1[i][k]*matrix2[k][j];
      }
      (*out)[i][j]=v;
    }
  }

  t2 = omp_get_wtime();
  return t2-t1;
}

double matrix_multiplication_parallel(int** matrix1, int** matrix2, const int n1, const int m1,int n2, const int m2,int*** out)
{
  double t1,t2;
    int chunkSize =10;
  *out = new int * [n1];
  for(int i=0;i<n1;i++) (*out)[i] = new int[m2];
  int ** a = *out;
  t1 = omp_get_wtime();
    int i,j,k;

#pragma omp parallel shared(a,matrix1,matrix2,chunkSize) private(i,j,k)
    {
#pragma omp for schedule (static, chunkSize)
        for (i=0; i<n1; i++)
        {
            for(j=0; j<m2; j++)
                for (k=0; k<m1; k++)
                    a[i][j] += matrix1[i][k] * matrix2[k][j];
        }
    }

  t2 = omp_get_wtime();
   return t2-t1;
}
