#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <string.h>
#include <omp.h>

int soe(int limit){

  omp_set_num_threads( omp_get_num_procs());
  const int sqrtN = (int)sqrt((double)limit);
  int memorySize = (limit-1)/2;
  char* array = new char[memorySize+1];
  #pragma omp parallel for
  for (int i = 0; i <= memorySize; i++)
    array[i] = 1;
    #pragma omp parallel for schedule(dynamic)
    for (int i = 3; i <= sqrtN; i += 2)
      if (array[i/2])
        for (int j = i*i; j <= limit; j += 2*i)
          array[j/2] = 0;
          int sol = limit >= 2 ? 1 : 0;
          #pragma omp parallel for reduction(+:sol)
          for (int i = 1; i <= memorySize; i++)
            sol += array[i];
            delete[] array;
            return sol;
}

int main(int argc,char ** argv){
  if(argc<2){
    printf("Use: %s %s\n",argv[0],"upperbound");
    exit(1);
  }
  int limit = atoi(argv[1]);

  double t1,t2;
  t1 = omp_get_wtime();
  printf("No of primes in range (2,%d): %d\n",limit,soe(limit));
  t2 = omp_get_wtime();
  printf("Time %.16g\n",t2-t1 );
  return 0;
}
