#include <stdlib.h>

int read_matrix(const char * filename,int *** matrix,int * n,int *m){
  FILE * file = fopen(filename,"r");
  if(file == NULL){
    return -1;
  }
  fscanf(file,"%d %d\n",n,m);
  printf("%s was read (%dx%d)\n",filename,*n,*m);
  if(!(*n >0 && *m>0)){
    return -1;
  }
  *matrix = new int *[*n];
  for(int i=0;i<*n;i++){
    (*matrix)[i] = new int[*m];
    for(int j=0;j<*m;j++){
      fscanf(file,"%d",&(*matrix)[i][j]);
    }
  }

  fclose(file);
}
