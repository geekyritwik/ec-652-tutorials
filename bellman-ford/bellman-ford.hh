#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>


struct Edge
{
    int src, dest, weight;
};

struct Graph
{

    int V, E;
    struct Edge* edge;
};

struct Graph* createGraph(int V, int E)
{
    struct Graph* graph = (struct Graph*) malloc( sizeof(struct Graph) );
    graph->V = V;
    graph->E = E;

    graph->edge = (struct Edge*) malloc( graph->E * sizeof( struct Edge ) );

    return graph;
}

struct Graph * createGraph(int V,int E,int ** topology){
  struct Graph* graph = createGraph(V,E);
  int edges = 0;
  for(int i=0;i<V && edges<E;i++){
    for(int j=0;j<V && edges<E;j++){
      if(topology[i][j] !=0){
        graph->edge[edges].src = i;
        graph->edge[edges].dest = j;
        graph->edge[edges].weight = topology[i][j];
        edges++;
      }
    }
  }
  return graph;
}

void printArr(int dist[], int V)
{
  for(int j=0;j<V;j++){
    printf("DV[%d] = %d ,",j,dist[j]);
 }
 printf("\n");
}


void BellmanFord(struct Graph* graph, int src)
{
    int V = graph->V;
    int E = graph->E;
    int dist[V];


    for (int i = 0; i < V; i++)
        dist[i]   = INT_MAX;
    dist[src] = 0;



    for (int i = 1; i <= V-1; i++)
    {
        for (int j = 0; j < E; j++)
        {
            int u = graph->edge[j].src;
            int v = graph->edge[j].dest;
            int weight = graph->edge[j].weight;
            if (dist[u] != INT_MAX && dist[u] + weight < dist[v])
                dist[v] = dist[u] + weight;
        }
    }

    for (int i = 0; i < E; i++)
    {
        int u = graph->edge[i].src;
        int v = graph->edge[i].dest;
        int weight = graph->edge[i].weight;
        if (dist[u] != INT_MAX && dist[u] + weight < dist[v])
            printf("Graph contains negative weight cycle");
    }

    printArr(dist, V);

    return;
}


#define MY_MAX 1000

int min(int a,int b){
  return (a>b)?b:a;
}

void printArray(int ** distVectors,int V,int rank){
    for(int j=0;j<V;j++){
      printf("DV[%d] = %d ,",j,distVectors[rank][j]);
   }
   printf("\n");
}

void Send(int hisIndex,int * value,int size){
  MPI_Send(value,size,MPI_INT,hisIndex,0,MPI_COMM_WORLD);
}

void Receive(int hisIndex,int * value,int size){
  MPI_Recv(value,size,MPI_INT,hisIndex,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
}

void bellman_ford_parallel(int argc,char ** argv,int ** topology,int ** distVectors,int V,int E,int debug){

  MPI_Init(&argc,&argv);

  int size;
  MPI_Comm_size(MPI_COMM_WORLD,&size);

  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  int tempVector[V+1];
  double t1,t2;

if(!debug){

if(0==rank)printf("No of Nodes %d\n",V);

}else{

  //Initially build distance vectors
  for(int i=0;i<V;i++){
    if(topology[rank][i]>0){
      distVectors[rank][i] = topology[rank][i];
    }else if(topology[rank][i] ==0){
      distVectors[rank][i] = MY_MAX;
    }
  }
  distVectors[rank][rank]= 0;
  distVectors[rank][V] = rank;
  //built distance vectors
  MPI_Barrier(MPI_COMM_WORLD);

  if(0 == rank){
    t1 = MPI_Wtime();
  }
  for(int i=0;i<V;i++){
    if(topology[rank][i]>0){
      Send(i,distVectors[rank],V+1);
    }
  }

  MPI_Barrier(MPI_COMM_WORLD);

  for(int i=0;i<V;i++){
    Receive(MPI_ANY_SOURCE,tempVector,V+1);
    int w = tempVector[V];
    for(int j=0;j<V;j++){
      distVectors[rank][j] = min(distVectors[rank][j],topology[rank][w]+tempVector[j]);

    }


    for(int k=0;k<V;k++){
      if(topology[rank][k]>0){
        Send(k,distVectors[rank],V+1);
      }
    }

    MPI_Barrier(MPI_COMM_WORLD);

  }



    if(rank == 0){
      t2 = MPI_Wtime();
      printArray(distVectors,V,rank);
      printf("Time Elapsed %f\n",t2-t1);
    }

}
  MPI_Finalize();

}

void bellman_ford_serial(int argc, char ** argv, int ** topology,int V,int E){

  MPI_Init(&argc,&argv);

  int size;
  MPI_Comm_size(MPI_COMM_WORLD,&size);

  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  if(size !=1){
    MPI_Finalize();
    exit(1);
  }

  Graph * graph = createGraph(V,E,topology);

	double t1 = MPI_Wtime();
	BellmanFord(graph, 0);
	double t2 = MPI_Wtime();
  printf("Time Elapsed %f\n",t2-t1);

  MPI_Finalize();
}
