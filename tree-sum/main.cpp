#include <cstdio>
#include <cstdlib>
#include <omp.h>
#include <iostream>
#include <cstring>
#include <time.h>
#include <cmath>
 using namespace std;
int main(int argc , char ** argv)
{
  int *arr, *partial, *temp;
  int num_threads, work, n;
  int i, mynum, last;
  char p;
   p = argv[1][0];
  printf("%c\n",p);
  n = atoi(argv[2]);

  double t1,t2;
  
  int depth = (int)log2(n); 

  if(!(arr = (int *) malloc (sizeof (int) * n))) return -1;
  for(i = 0; i < n; i++)
    arr[i] = i+1;  
     clock_t    start;
     start = clock();
     int k ;

t1 = omp_get_wtime();

  for (k = depth-1; k >=0 ; k--)
{    
    int index = (int)((int)pow(2 ,k+1) - 2);
    int index2;
    if(k==0) index2=0;
    index2 = (int)pow(2, k)-1;
    int j ;
    #pragma omp parallel for
    for(j = index ; j >= index2 ; j--)  
    {if (2*j+1<=n-1) arr[j] += arr[2*j+1];  if(2*j+2<=n-1) arr[j] += arr[2*j+2];} 
}

t2 = omp_get_wtime();
double t = t2 -t1;

printf( "Time: %.16g ms \n" , t);

  for(i = 0; i < n; i++)
    printf("%d ", arr[i]);
  printf("\n");
  return 0;


}
