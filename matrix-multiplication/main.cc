#include <iostream>
#include "matrix-mul.hh"
#include "read-matrix.hh"
#include "write-matrix.hh"

using namespace std;

int main(int argc, char ** argv){
  int n1,m1,n2,m2,r1,r2;
  int **matrix1,**matrix2,** out,**out2;
  double timeout1,timeout2;

  r1 = read_matrix("matrix.txt",&matrix1,&n1,&m1);
  r2 = read_matrix("matrix.txt",&matrix2,&n2,&m2);
  if(r1<0 || r2<0){
    printf("Invalid Files\n");
    exit(1);
  }
  timeout1 = matrix_multiplication_serial(matrix1,matrix2,n1,m1,n2,m2,&out);
  timeout2 = matrix_multiplication_parallel(matrix1,matrix2,n1,m1,n2,m2,&out2);

  cout<<"Serial Execution Time: "<<timeout1<<endl;
  cout<<"Parallel Execution Time: "<<timeout2<<endl;

  printf("Wait! Writing Results......\n");
  write_matrix(out,n1,m2,"matrix-result-serial.txt");
  write_matrix(out2,n1,m2,"matrix-result-parallel.txt");
  printf("Done!!\n");
  if(matrix1){
    delete [] matrix1;
  }
  if(matrix2){
    delete [] matrix2;
  }
  if(out){
    delete [] out;
  }
  if(out2){
    delete out2;
  }
}
