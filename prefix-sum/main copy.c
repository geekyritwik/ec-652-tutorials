#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <string.h>
#include <time.h>
 
int main(int argc ,char ** argv)
{
  if(argc <3){
    printf("Usage %s %s %s\n",argv[0],"parallel (y or n)","array_size");
  }
  int *arr, *partial, *temp;
  int num_threads, work, n;
  int i, mynum, last;
  char p;
  p = argv[1][0];
  printf("%c\n",p);

  n = atoi(argv[2]);
  
  if(!(arr = (int *) malloc (sizeof (int) * n))) return -1;

  for(i = 0; i < n; i++)arr[i] = i+1;
     clock_t    start;
     start = clock();
     double t1,t2;
  
  int no_of_threads;

  t1 = omp_get_wtime();
#pragma omp parallel default(none) private(i, mynum, last) shared(arr, partial, temp, num_threads, work, n,p)
  {
#pragma omp single
    {
      
      if(p=='y')
      num_threads = omp_get_num_threads();
      else num_threads=1;
      printf("no of threads :%d" , num_threads);
      if(!(partial = (int *) malloc (sizeof (int) * num_threads))) exit(-1);
      if(!(temp = (int *) malloc (sizeof (int) * num_threads))) exit(-1);
      work = n / num_threads + 1;
    }
    mynum = omp_get_thread_num();
    for(i = work * mynum + 1; i < work * mynum + work && i < n; i++)
      arr[i] += arr[i - 1];
    partial[mynum] = arr[i - 1];
#pragma omp barrier
    for(i = 1; i < num_threads; i <<= 1) {
      if(mynum >= i)
        temp[mynum] = partial[mynum] + partial[mynum - i];
#pragma omp barrier
#pragma omp single
      memcpy(partial + 1, temp + 1, sizeof(int) * (num_threads - 1));
    }
    for(i = work * mynum; i < (last = work * mynum + work < n ? work * mynum + work : n); i++)
      arr[i] += partial[mynum] - arr[last - 1];
  }
    t2 = omp_get_wtime();
double t = (clock() - start) / (double) (CLOCKS_PER_SEC / 1000)/num_threads;
//double t = t2 -t1;

printf( "Time: %.16g ms \n" , t);
  for(i = 0; i < n; i++)
    printf("%d\n", arr[i]);
  return 0;
}
