#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include <sys/time.h>
 
int main(int argc,char ** argv){

   int len = atoi(argv[1]);
  int * array = (int *) malloc(len * sizeof(int));
  for(int i=0;i<len;i++){
    array[i] = i+1;
  }
   double t1,t2;
   t1 = omp_get_wtime();
   for(int i=1;i<len;i++){
      array[i] += array[i-1];
   }

   t2 = omp_get_wtime();

   for (int i = 0; i < len; ++i)
   {
      printf("%d\n",array[i]);
   }

   printf("%.16g\n",t2-t1 );

  return 0;
}
