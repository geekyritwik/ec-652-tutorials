#include <mpi.h>
#include <iostream>
#include "bellman-ford.hh"
#include "topology.hh"
using namespace std;

int main(int argc,char** argv){
	int ** topology,V,E;

	int parallel = atoi(argv[1]);
	readTopology("topology.txt",&topology,&V,&E);

	if(!parallel){
		bellman_ford_serial(argc,argv,topology,V,E);
	}else{
		int ** distVectors = new int*[V];
	  for(int i=0;i<V;i++){
	    distVectors[i] = new int[V+1];
	  }
	   bellman_ford_parallel(argc,argv,topology,distVectors,V,E,parallel);

	}

}
